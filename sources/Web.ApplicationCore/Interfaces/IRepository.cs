﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Web.ApplicationCore.Interfaces
{
    public interface IRepository<T> where T : class
    {
        Task<IReadOnlyList<T>> Get();
        Task<T> Find(params object[] values);
        Task<T> Add(T entity);
        Task Update(T entity);
        Task Delete(T entity);
    }
}
