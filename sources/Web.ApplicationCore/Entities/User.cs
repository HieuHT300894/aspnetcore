﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Web.ApplicationCore.Entities
{
    public class User
    {
        public Guid Id { get; set; }

        public string Password { get; set; }

        public bool IsActive { get; set; }
    }
}
