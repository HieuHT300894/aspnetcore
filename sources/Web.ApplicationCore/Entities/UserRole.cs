﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Web.ApplicationCore.Entities
{
    public class UserRole
    {
        public Guid UserId { get; set; }

        public Guid RoleId { get; set; }
    }
}
