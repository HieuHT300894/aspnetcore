﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Web.ApplicationCore.Entities
{
    class RolePermission
    {
        public Guid RoleId { get; set; }

        public Guid PermissionId { get; set; }
    }
}
